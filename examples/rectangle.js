var p = [[[0], [0]], [[1], [0]], [[1], [1]], [[0], [1]]];
var Scale = [[100, 0], [0, 100]];
var Offset = [[100], [100]];
var T = [[-0.5], [-0.5]];
var Tneg = m_neg(T);
function setup() {
  createCanvas(500, 500);
  for (var i = 0; i < 4; i++) {
    p[i] = m_mul(Scale, p[i]);
    p[i] = m_sum(Offset, p[i]);
  }
  T = m_mul(Scale, T);
  T = m_sum(m_neg(Offset), T);
  Tneg = m_neg(T);
}

function draw() {
  background(255);
  for (var i = 0; i < 4; i++) {
    p[i] = m_sum(Tneg, m_mul(Rot(0.01), m_sum(T, p[i])));
  }
  line(p[0][0][0], p[0][1][0], p[1][0][0], p[1][1][0]);
  line(p[1][0][0], p[1][1][0], p[2][0][0], p[2][1][0]);
  line(p[2][0][0], p[2][1][0], p[3][0][0], p[3][1][0]);
  line(p[3][0][0], p[3][1][0], p[0][0][0], p[0][1][0]);
}