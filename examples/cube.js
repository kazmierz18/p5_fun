var p = [
  [[0], [0], [0]], [[1], [0], [0]], [[1], [1], [0]], [[0], [1], [0]],
  [[0], [0], [1]], [[1], [0], [1]], [[1], [1], [1]], [[0], [1], [1]]
];
var Scale = [[100, 0, 0], [0, 100, 0], [0, 0, 100]];
var Offset = [[100], [100], [100]];
var T = [[-0.5], [-0.5], [0]];
var Tneg = m_neg(T);
function setup() {
  createCanvas(500, 500);
  for (var i = 0; i < 8; i++) {
    p[i] = m_mul(Scale, p[i]);
    p[i] = m_sum(Offset, p[i]);
  }
  T = m_mul(Scale, T);
  T = m_sum(m_neg(Offset), T);
  Tneg = m_neg(T);
}

function draw() {
  background(255);
  pp = [];
  for (var i = 0; i < 8; i++) {
    p[i] = m_sum(Tneg, m_mul(RotX(0.001),m_mul(RotZ(0.01), m_sum(T, p[i]))));
    pp.push(project_3D_to_2D(p[i]));
  }
  line(pp[0][0][0], pp[0][1][0], pp[1][0][0], pp[1][1][0]);
  line(pp[1][0][0], pp[1][1][0], pp[2][0][0], pp[2][1][0]);
  line(pp[2][0][0], pp[2][1][0], pp[3][0][0], pp[3][1][0]);
  line(pp[3][0][0], pp[3][1][0], pp[0][0][0], pp[0][1][0]);

  line(pp[4][0][0], pp[4][1][0], pp[5][0][0], pp[5][1][0]);
  line(pp[5][0][0], pp[5][1][0], pp[6][0][0], pp[6][1][0]);
  line(pp[6][0][0], pp[6][1][0], pp[7][0][0], pp[7][1][0]);
  line(pp[7][0][0], pp[7][1][0], pp[4][0][0], pp[4][1][0]);
  
  line(pp[0][0][0], pp[0][1][0], pp[4][0][0], pp[4][1][0]);
  line(pp[1][0][0], pp[1][1][0], pp[5][0][0], pp[5][1][0]);
  line(pp[2][0][0], pp[2][1][0], pp[6][0][0], pp[6][1][0]);
  line(pp[3][0][0], pp[3][1][0], pp[7][0][0], pp[7][1][0]);
}