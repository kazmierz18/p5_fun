// Step 1: Create an image classifier with MobileNet
var classifier;
var resultP;

function preload() {
    const options = { probabilityThreshold: 0.7 };
    classifier = ml5.soundClassifier('SpeechCommands18w', options);

}
function setup() {
    noCanvas();
    resultP = createP('waiting...');
    resultP.style('font-size', '32pt');
    classifier.classify(gotResults);
}

function gotResults(err, results) {
    if (err) {
        console.error(err)
    }
    resultP.html(`${results[0].label} : ${results[0].confidence}`);
}

function addWord() {
    const recognizer = speechCommands.create('BROWSER_FFT');
}