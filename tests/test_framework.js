function assert(msg, expr, expect) {
  var el = document.createElement('P');
  if (expr != expect) {
    el.innerHTML = 'Assertion failed: "' + msg + '" value:' + expr +
        ' expected: ' + expect;
    el.classList.add('a_fail');
    // throw 'Assertion failed: "' + msg + '" value:' + expr +
    //     ' expected: ' + expect;
  } else {
    el.innerHTML = msg + ' OK';
    el.classList.add('a_success');
  }
  document.body.appendChild(el);
}


function in_range(x, expected, error) {
  return x <= expected + error && x >= expected - error;
}


function m_assert(msg, expr, expect, error = 0) {
  var el = document.createElement('P');
  if (!m_eq(expr, expect, error)) {
    el.innerHTML = 'Assertion failed: "' + msg + '" value:' + expr +
        ' expected: ' + expect;
    el.classList.add('a_fail');
    // throw 'Assertion failed: "' + msg + '" value:' + expr +
    //     ' expected: ' + expect;
  } else {
    el.innerHTML = msg + ' OK';
    el.classList.add('a_success');
  }
  document.body.appendChild(el);
}
