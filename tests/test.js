
function setup() {
    // put setup code here
    noCanvas();
    let A = [[1, 2], [3, 4]];
    let B = [[1], [3]];
    let B2 = [[2], [6]];
    let AB = [[7], [15]];
    let A2 = [[7, 10], [15, 22]];
    let I = [[1, 0], [0, 1]];
    let Ineg = [[-1, 0], [0, -1]];
    let Ones = [[1, 1], [1, 1]];
    let Zeros = [[0, 0], [0, 0]];
    let ApI = [[2, 2], [3, 5]];
    // Confirm basic math is working
    m_assert('m_ones(2)', m_ones(2), Ones);
    m_assert('m_zeros(2)', m_zeros(2), Zeros);
    m_assert('m_eye(2)', m_eye(2), I);
    m_assert('m_neg(I)', m_neg(I), Ineg);
    m_assert('m_sum(A, Zeros)', m_sum(A, Zeros), A);
    m_assert('m_sum(Zeros, Zeros)', m_sum(Zeros, Zeros), Zeros);
    m_assert('m_sum(I, m_neg(I))', m_sum(I, m_neg(I)), Zeros);
    m_assert('m_sum(A, I)', m_sum(A, I), ApI);
    m_assert('m_sum(B, B)', m_sum(B, B), B2);
    m_assert('m_mul(Zeros, Zeros))', m_mul(Zeros, Zeros), Zeros);
    m_assert('m_mul(I, Zeros))', m_mul(I, Zeros), Zeros);
    m_assert('m_mul(I, I))', m_mul(I, I), I);
    m_assert('m_mul(A, I))', m_mul(A, I), A);
    m_assert('m_mul(I, A))', m_mul(I, A), A);
    m_assert('m_mul(A, A))', m_mul(A, A), A2);
    m_assert('m_mul(A, B))', m_mul(A, B), AB);
    // now test some basic rotations are working
    let P0 = [[1], [0]];
    let P1 = [[0], [1]];
    let P3 = [[0], [1], [0]];
    let P4 = [[0], [0], [1]];
    m_assert('m_mul(Rot(pi/2), P0))', m_mul(Rot(Math.PI / 2), P0), P1, 0.01);
    m_assert('m_mul(RotX(pi/2), P3))', m_mul(RotX(Math.PI / 2), P3), P4, 0.01);

    // now test some ray casting
    const b = new Border(new p5.Vector(0, 0), new p5.Vector(10, 0));
    const r = new Ray(new p5.Vector(0, 10), 0);
    r.cast(b);
    assert('r.cast(border) -> x', r.p.x, 0, 0.01);
    assert('r.cast(border) -> y', r.p.y, 0, 0.01);
}