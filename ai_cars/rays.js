class Ray {
    constructor(position, angle) {
        this.pos = position;
        this.heading = new p5.Vector.fromAngle(angle).mult(100);
        this.p=new p5.Vector(0,0);
    }

    draw() {
        stroke(100, 100);
        line(0, 0, this.heading.x, this.heading.y);
        strokeWeight(10);
        stroke(10,255,100);
        point(this.p.x,this.p.y);
        strokeWeight(1);
    }

    cast(wall) {
        const den = (this.pos.x - this.heading.x) * (wall.p1.y - wall.p2.y)
            - (this.pos.y - this.heading.y) * (wall.p1.x - wall.p2.x);
        if (den != 0) {
            const t = (this.pos.x - wall.p2.x) * (wall.p1.y - wall.p2.y)
                - (this.pos.y - wall.p2.y) * (wall.p1.x - wall.p2.x);
            const u = (this.pos.x - this.heading.x) * (wall.p1.y - wall.p2.y)
                - (this.pos.y - this.heading.y) * (wall.p1.x - wall.p2.x);
            let p = new p5.Vector(wall.p1.x + u * (wall.p2.x - wall.p1.x),
                wall.p1.y + u * (wall.p2.y - wall.p1.y))
            this.p=p;
        }
    }
}