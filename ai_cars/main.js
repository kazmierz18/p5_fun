const SCREEN_WIDTH = 500;
const SCREEN_HEIGHT = 500;
const MIDDLE_X = SCREEN_WIDTH / 2;
const MIDDLE_Y = SCREEN_HEIGHT / 2;

POPULATION_SIZE = 1;

let track = []
let cars = [];
let ga = undefined;

function setup() {
    tf.setBackend('cpu');
    createCanvas(SCREEN_WIDTH, SCREEN_HEIGHT);
    track = new Track();
    ga = new GA();
    ga.populate();
    for (let i = 0; i < POPULATION_SIZE; i++) {
        cars.push(new Car(track.start_position()));
    }
}

function draw() {
    background(0);
    push()
    translate(MIDDLE_X, MIDDLE_Y);
    track.draw();
    for (let i = 0; i < POPULATION_SIZE; i++) {
        // const steering = ga.population[i].predict(cars[i].get_state());
        // cars[i].steer(steering[0], steering[1]);
        cars[i].drive();
        ga.population[i].timer(cars[i].checkpoint_check(track.checkpoints));
        cars[i].draw();
        for (const border of track.borders){
            cars[i].crashed(border);
        }
    }
    pop()
    // if (ga.all_dead()) {
    //     ga.find_best(cars);
    //     ga.get_next_generation();
    //     for (car of cars){
    //         car.reset();
    //     }
    // }
}

function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        cars[0].steer(-0.1, 0);
    } else if (keyCode === RIGHT_ARROW) {
        cars[0].steer(0.1, 0);
    } else if (keyCode === UP_ARROW) {
        cars[0].steer(0, 0.1);
    } else if (keyCode === DOWN_ARROW) {
        cars[0].steer(0, -0.1);
    } else {
        cars[0].steer(0, 0);
    }
}