const CAR_WIDTH = 10;
const CAR_HEIGHT = 5;
const VELOCITY_LIMIT = 2;
const ACCELERATION_LIMIT = 0.5;

function lp_distance(line, point) {
    return abs((line.p2.y - line.p1.y) * point.x - (line.p2.x - line.p1.x) * point.y + line.p2.x * line.p1.y - line.p2.y * line.p1.x) /
        sqrt(pow((line.p2.y - line.p1.y), 2) + pow(line.p2.x - line.p1.x, 2));
}

class Car {
    constructor(position) {
        this.start_position = position[0];
        this.position = position;
        this.velocity = 0;
        this.heading = new p5.Vector(0, position[1]);
        this.checkpoint_idx = 0;
        this.rays = [];
        for (let i = 0; i < 2; i++) {
            const angle = -PI / 8 + i * PI / 4;
            this.rays.push(new Ray(this.position, angle));
        }
    }

    update_position(dx, dy) {
        this.position.x += dx;
        this.position.y += dy;
    }

    drive() {
        const dx = cos(this.heading.heading()) * this.velocity
        const dy = sin(this.heading.heading()) * this.velocity
        this.update_position(dx, dy);
    }

    steer(dir, acceleration) {
        this.velocity += constrain(acceleration, -ACCELERATION_LIMIT, ACCELERATION_LIMIT);
        this.velocity = constrain(this.velocity, 0, VELOCITY_LIMIT);
        this.heading.rotate(dir);
    }

    crash_check(borders) {
        for (let border of borders) {
            if (lp_distance(border, this.position) < CAR_HEIGHT) {
                console.log("crashed", lp_distance(border, this.position));
                return;
            }
        }
    }

    checkpoint_check(checkpoints) {
        if (lp_distance(checkpoints[this.checkpoint_idx % checkpoints.length], this.position) < CAR_HEIGHT) {
            this.checkpoint_idx++;
            return true;
        }
        return false;
    }

    get_state() {
        return [this.position.x, this.position.y, this.velocity];
    }

    get_fitness() {
        return this.checkpoint_idx;
    }

    reset() {
        this.position = this.start_position.copy();
        this.velocity = 0;
        this.heading.x = 0;
        this.heading.y = -1;
        this.checkpoint_idx = 0;
    }

    draw() {
        stroke(200);
        push()
        translate(this.position.x, this.position.y)
        rotate(this.heading.heading());
        rect(-CAR_WIDTH / 2, -CAR_HEIGHT, CAR_WIDTH, CAR_HEIGHT);
        translate(0, -CAR_HEIGHT / 2);
        for (const ray of this.rays) {
            ray.draw();
        }
        pop();
    }

    crashed(wall){
        for(let ray of this.rays){
            ray.cast(wall);
        }
    }
}