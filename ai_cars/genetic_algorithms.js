class GA {
    constructor() {
        this.generation_cnt = 0;
        this.population = []
        this.current_best = undefined;
    }
    populate() {
        for (let i = 0; i < POPULATION_SIZE; i++) {
            this.population[i] = new NeuralNetwork(3, 5, 2);
            this.population[i].model=this.population[i].create_model();
        }
        this.current_best = this.population[0];
    }
    find_best(items) {
        for (let i = 0; i < items.length; i++) {
            if (items[i].get_fitness() > this.current_best.fitness) {
                this.current_best = this.population[i];
                this.current_best.fitness = items[i].get_fitness();
            }
        }
       console.log("best", this.current_best.fitness);
    }
    all_dead() {
        for (const brain of this.population) {
            if (brain.alive > 0) {
                return false;
            }
        }
        return true;
    }
    get_next_generation() {
        this.generation_cnt++;
        this.population[0] = this.current_best.copy();
        for (let i=1;i<this.population.length;i++){
            this.population[i]=this.population[0].copy();
            this.population[i].mutate(0.01);
        } 
        console.log("next generation", this.generation_cnt);
    }
}