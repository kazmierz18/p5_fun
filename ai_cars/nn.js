const LIFE_SPAN = 20;
class NeuralNetwork {
    constructor(inputs, hidden, output) {
        this.input_nodes = inputs;
        this.hidden_nodes = hidden;
        this.output_nodes = output;
        this.fitness = 0;
        this.alive = LIFE_SPAN;
        this.model = undefined;
    }

    mutate(rate) {
        tf.tidy(() => {
            const weights = this.model.getWeights();
            let mutatedWeights = [];
            for (let i = 0; i < weights.length; i++) {
                let tensor = weights[i];
                let shape = weights[i].shape;
                let values = tensor.dataSync().slice();
                for (let j = 0; j < values.length; j++) {
                    if (random(1) < rate) {
                        values[j] = values[j] + randomGaussian();
                    }
                }
                mutatedWeights[i] = tf.tensor(values, shape);
            }
            this.model.setWeights(mutatedWeights);
        });
    }

    copy() {
        return tf.tidy(() => {
            const modelCopy = this.create_model();
            const weights = this.model.getWeights();
            const weightsCopies = [];
            for (let i = 0; i < weights.length; i++) {
                weightsCopies[i] = weights[i].clone();
            }
            modelCopy.setWeights(weights);
            let nn = new NeuralNetwork(this.input_nodes, this.hidden_nodes, this.output_nodes);
            nn.model = modelCopy;
            return nn;
        });
    }

    kill(){
        alive=0;
    }

    timer(checkpoint) {
        if (checkpoint) {
            this.alive = LIFE_SPAN;
        }
        if (this.alive > 0) {
            this.alive--;
        }
    }
    predict(inputs) {
        return tf.tidy(() => {
            const xs = tf.tensor2d([inputs]);
            const ys = this.model.predict(xs);
            const outputs = ys.dataSync();
            return outputs;
        });
    }

    create_model() {
        let model = tf.sequential();
        const hidden = tf.layers.dense({
            units: this.hidden_nodes,
            inputShape: [this.input_nodes],
            activation: 'sigmoid'
        });
        model.add(hidden);
        const output = tf.layers.dense({
            units: this.output_nodes,
            activation: 'sigmoid'
        });
        model.add(output);
        return model;
    }

}