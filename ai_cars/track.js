class Border {
    constructor(p1, p2) {
        this.p1 = p1;
        this.p2 = p2;
    }
}

class Track {
    constructor() {
        this.borders = [];
        this.checkpoints = [];
        const SEGMENTS = 30
        let checkpoints = [];
        const TRACK_WIDTH = 40
        const NOISE_SCALE = 0.5;
        for (let i = 0; i < SEGMENTS; i++) {
            const angle = TWO_PI * i / SEGMENTS + PI;
            const r = noise(angle * NOISE_SCALE, angle * NOISE_SCALE) * 200;
            const x = r * cos(angle);
            const y = r * sin(angle);
            checkpoints.push([new p5.Vector(x, y), new p5.Vector(x + cos(angle) * TRACK_WIDTH, y + sin(angle) * TRACK_WIDTH)]);
        }
        for (let i = 0; i < checkpoints.length - 1; i++) {
            this.borders.push(new Border(checkpoints[i][0], checkpoints[i + 1][0]))
            this.borders.push(new Border(checkpoints[i][1], checkpoints[i + 1][1]))
        }

        this.borders.push(new Border(checkpoints[checkpoints.length - 1][0], checkpoints[0][0]))
        this.borders.push(new Border(checkpoints[checkpoints.length - 1][1], checkpoints[0][1]))

        // create checkpoint lines
        for (const checkpoint of checkpoints) {
            this.checkpoints.push(new Border(checkpoint[0], checkpoint[1]))
        }
    }

    start_position() {
        const x = (this.checkpoints[0].p1.x + this.checkpoints[0].p2.x) / 2;
        const y = (this.checkpoints[0].p1.y + this.checkpoints[0].p2.y) / 2;
        return [new p5.Vector(x, y),-1];
    }

    draw() {
        //draw borders
        stroke(255);
        for (const border of this.borders) {
            line(border.p1.x, border.p1.y, border.p2.x, border.p2.y);
        }
        stroke(50);
        for (const checkpoint of this.checkpoints) {
            line(checkpoint.p1.x, checkpoint.p1.y, checkpoint.p2.x, checkpoint.p2.y);
        }
        stroke(250, 50, 50)
        line(this.checkpoints[0].p1.x, this.checkpoints[0].p1.y, this.checkpoints[0].p2.x, this.checkpoints[0].p2.y);
    }
}

class StraightTrack {
    constructor() {
        this.borders = [];
        this.checkpoints = [];
        const SEGMENTS = 20
        let checkpoints = [];
        const TRACK_WIDTH = 50
        const NOISE_SCALE = 0.5;
        for (let i = 0; i < SEGMENTS; i++) {
            const offset = TWO_PI * i / SEGMENTS + PI;
            const r = noise(offset * NOISE_SCALE, offset * NOISE_SCALE) * 200;
            const x = i * 20 - 200;
            const y = r;
            checkpoints.push([new p5.Vector(x, y), new p5.Vector(x, y + TRACK_WIDTH)]);
        }
        for (let i = 0; i < checkpoints.length - 1; i++) {
            this.borders.push(new Border(checkpoints[i][0], checkpoints[i + 1][0]))
            this.borders.push(new Border(checkpoints[i][1], checkpoints[i + 1][1]))
        }

        // create checkpoint lines
        for (const checkpoint of checkpoints) {
            this.checkpoints.push(new Border(checkpoint[0], checkpoint[1]))
        }
    }

    start_position() {
        const x = (this.checkpoints[0].p1.x + this.checkpoints[0].p2.x) / 2;
        const y = (this.checkpoints[0].p1.y + this.checkpoints[0].p2.y) / 2;
        return [new p5.Vector(x, y), -1];
    }

    draw() {
        //draw borders
        stroke(255);
        for (const border of this.borders) {
            line(border.p1.x, border.p1.y, border.p2.x, border.p2.y);
        }
        stroke(50);
        for (const checkpoint of this.checkpoints) {
            line(checkpoint.p1.x, checkpoint.p1.y, checkpoint.p2.x, checkpoint.p2.y);
        }
        stroke(250, 50, 50)
        line(this.checkpoints[0].p1.x, this.checkpoints[0].p1.y, this.checkpoints[0].p2.x, this.checkpoints[0].p2.y);
    }
}