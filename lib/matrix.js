function m_chk_dim(a, b, t = true) {
  if (t) {
    if (a[0].length != b[0].length && a.length != b.length) {
      throw 'Array dimension mismatch';
    }
    return
  }
  // special case for multiplication
  if (a[0].length != b.length) {
    throw 'Array dimension mismatch';
  }
}

function m_eq(a, b, error = 0) {
  m_chk_dim(a, b);
  for (var i = 0; i < a.length; i++) {
    for (var j = 0; j < a[0].length; j++) {
      if (!in_range(a[i][j], b[i][j], error)) {
        return false;
      }
    }
  }
  return true;
}

function m_create(width, height, fill) {
  var ret = new Array(height);
  for (var i = 0; i < ret.length; i++) {
    ret[i] = new Array(width);
    for (var j = 0; j < ret[i].length; j++) {
      ret[i][j] = fill;
    }
  }
  return ret;
}

function m_ones(size) {
  return m_create(size, size, 1);
}

function m_zeros(size) {
  return m_create(size, size, 0);
}

function m_eye(size) {
  var ret = m_create(size, size, 0);
  for (var i = 0; i < ret.length; i++) {
    ret[i][i] = 1;
  }
  return ret;
}

function m_neg(a) {
  var ret = m_create(a[0].length, a.length, 0);
  for (var i = 0; i < a.length; i++) {
    for (var j = 0; j < a[0].length; j++) {
      ret[i][j] = -a[i][j];
    }
  }
  return ret;
}

function m_sum(a, b) {
  m_chk_dim(a, b);
  var ret = m_create(a[0].length, a.length, 0);
  for (var i = 0; i < a.length; i++) {
    for (var j = 0; j < a[0].length; j++) {
      ret[i][j] = a[i][j] + b[i][j];
    }
  }
  return ret;
}

function m_mul(a, b) {
  m_chk_dim(a, b, false);
  var ret = m_create(b[0].length, a.length, 0);
  for (var j = 0; j < ret.length; j++) {
    for (var i = 0; i < ret[0].length; i++) {
      for (var ii = 0; ii < a[0].length; ii++) {
        ret[j][i] += a[j][ii] * b[ii][i];
      }
    }
  }
  return ret;
}


function Rot(alpha) {
  return [
    [Math.cos(alpha), -Math.sin(alpha)], [Math.sin(alpha), Math.cos(alpha)]
  ];
}

function RotX(alpha) {
  return [
    [1, 0, 0], [0, Math.cos(alpha), -Math.sin(alpha)],
    [0, Math.sin(alpha), Math.cos(alpha)]
  ];
}

function RotY(alpha) {
  return [
    [Math.cos(alpha), 0, Math.sin(alpha)], [0, 1, 0],
    [-Math.sin(alpha), 0, Math.cos(alpha)]
  ];
}

function RotZ(alpha) {
  return [
    [Math.cos(alpha), -Math.sin(alpha), 0],
    [Math.sin(alpha), Math.cos(alpha), 0], [0, 0, 1]
  ];
}

function RotXY(alpha) {
  return [
    [Math.cos(alpha), Math.sin(alpha), 0, 0],
    [-Math.sin(alpha), Math.cos(alpha), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]
  ];
}

function RotYZ(alpha) {
  return [
    [1, 0, 0, 0], [0, Math.cos(alpha), -Math.sin(alpha), 0],
    [0, -Math.sin(alpha), Math.cos(alpha), 0], [0, 0, 0, 1]
  ];
}

function RotXZ(alpha) {
  return [
    [Math.cos(alpha), 0, -Math.sin(alpha), 0], [0, 1, 0, 0],
    [Math.sin(alpha), 0, Math.cos(alpha), 0], [0, 0, 0, 1]
  ];
}

function RotXU(alpha) {
  return [
    [Math.cos(alpha), 0, 0, Math.sin(alpha)],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [-Math.sin(alpha), 0, 0, Math.cos(alpha)],
  ];
}

function RotYU(alpha) {
  return [
    [1, 0, 0, 0],
    [0, Math.cos(alpha), 0, -Math.sin(alpha)],
    [0, 0, 1, 0],
    [Math.sin(alpha), 0, Math.cos(alpha)],
  ];
}

function RotZU(alpha) {
  return [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, Math.cos(alpha), -Math.sin(alpha)],
    [0, 0, Math.sin(alpha), Math.cos(alpha)],
  ];
}

function project_3D_to_2D(p) {
  var s = [[1, 0, 0], [0, 1, 0]];
  var c = [[1], [1]];
  return m_sum(m_mul(s, p), c);
}

function project_4D_to_3D(p) {
  var s = [[1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
  var c = [[1], [1], [1]]
  return m_sum(m_mul(s, p), c);
}